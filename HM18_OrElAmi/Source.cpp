#include <iostream>
#include <string>
#include <vector>
#include <Windows.h>
#include "Helper.h"

#define PATH_MAX 2048

using namespace std;

void create(LPCSTR);
vector<string> ls(string);
void secret();
void executable(LPCTSTR);

typedef int (CALLBACK* SecretFunc)();

int main()
{
	Helper h;
	string input, pathS;
	vector<string> words;
	char pathC[PATH_MAX];
	GetModuleFileName(NULL, pathC, PATH_MAX);
	pathS = string(pathC);
	while (true)
	{
		getline(cin, input);
		h.trim(input);
		words = h.get_words(input);
		if (words.size() == (unsigned int)2)
		{
			if (words[0] == "cd") pathS = words[1];
			if (words[0] == "create") create((pathS + "/" + words[1]).c_str());
		}
		else if (words.size() == (unsigned int)1)
		{
			if (words[0] == "pwd") cout << pathS << endl;
			if (words[0] == "ls") for (const auto& i : ls(pathS + "/*")) std::cout << i << endl;
			if (words[0] == "secret") secret();
			for (const auto& i : ls(pathS + "/*")) if (i == words[0]) executable((pathS + "/" + i).c_str());
		}
	}
}

void create(LPCSTR fileName)
{
	HANDLE hndl = CreateFile(fileName, GENERIC_WRITE, 0, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (hndl) CloseHandle(hndl);
	else cout << "create failed:" << GetLastError() << "\n";
}

vector<string> ls(string pathS)
{
	LPCSTR dirName = pathS.c_str();
	vector<string> files;
	WIN32_FIND_DATA data;
	HANDLE hndl = FindFirstFile(dirName, &data);
	if (hndl != INVALID_HANDLE_VALUE)
	{
		do { files.push_back(data.cFileName); } while (FindNextFile(hndl, &data));
		FindClose(hndl);
	}
	return files;
}

void secret()
{
	HINSTANCE dllHandle = LoadLibrary("Secret.dll");
	if (dllHandle != NULL)
	{
		SecretFunc secretFunc = (SecretFunc) GetProcAddress(dllHandle, "TheAnswerToLifeTheUniverseAndEverything");
		if (NULL != secretFunc) cout << secretFunc() << endl;
		FreeLibrary(dllHandle);
	}
}

void executable(LPCTSTR exeFile)
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	ZeroMemory(&pi, sizeof(pi));
	CreateProcess(exeFile, NULL, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi);
	CloseHandle(pi.hProcess);
	CloseHandle(pi.hThread);
}